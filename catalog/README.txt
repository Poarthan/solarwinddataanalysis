LPF External Force/Torque Data
This directory contains a catalog of estimated external forces and
torques on the LISA Pathfinder Spacecraft.  They were computed from a
combination of displacement sensor telemetry, actuator telemetry, and
knowledge of the spacecraft control systems and mass properties as
described in seciton 3.1 of https://doi.org/10.3847/1538-4357/ab3649.
The data is restricted to times when the spacecraft was in a "noise run"
configuration with no deliberate external or internal excitations added
to the spacecraft or instrument. Data filenames have the following
format:

g<number>_<DOF>_<T0>_<DUR>.txt  where:
<number> refers to which gravitational reference sensor (1 or 2) was
used to compute the data. In principle, the measurements should be the
same, but there may be slight differences in the noise and/or
calibration bias.

<DOF> refers to the kinematic degree of freedom according to the
following coordinate system:
z - top deck of the spacecraft, typically oriented to the Sun within
2deg
x - points from test mass 1 to test mass 2 (spacecraft rotates with 6mo
period)
y - completes right hand triad
theta, eta, phi - right-hand rotations around x,y,z

<T0> start time of the segment in GPS seconds
<DUR> duration of the segment

The data in each files are Fourier transforms of these signals in three
columns: frequency, real part, imaginary part.  Linear DOF timeseries
were in meters/s^2, angular DOF timeseries were in radians/s^2.

