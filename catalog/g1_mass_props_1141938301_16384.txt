// S/C Mass Properties used for g1 segment with t0 = 1141938301
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.000867	0.0027	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.787	-6.6
-0.787	 239	-2.96
-6.6	-2.96	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.22	-0.0329
-1.22	 240	-2.96
-0.0327	-2.96	 217
