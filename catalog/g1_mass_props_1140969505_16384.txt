// S/C Mass Properties used for g1 segment with t0 = 1140969505
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.000639	0.00287	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.773	-6.56
-0.773	 239	-2.99
-6.56	-2.99	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.23	0.013
-1.23	 240	-2.99
0.0132	-2.99	 217
