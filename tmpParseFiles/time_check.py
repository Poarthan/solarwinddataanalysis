#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
from datetime import datetime, timedelta
import time 
import math
from tqdm import tqdm


with open('2000_time_seconds.txt') as file:
    lines = file.readlines()
    lines = [line.rstrip() for line in lines]

for i in tqdm(range(len(lines))):
    if float(lines[i+1])-float(lines[i])<0:
        pritn(lines[i+1], lines[i])
